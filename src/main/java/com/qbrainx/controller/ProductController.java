package com.qbrainx.controller;

import com.qbrainx.domain.Product;
import com.qbrainx.service.ProductService;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public Iterable<Product> list() {
        return productService.listAllProducts();
    }

    @RequestMapping(value = "product/{id}", method = RequestMethod.GET)
    public Product showProduct(@PathVariable final Integer id) {
        return productService.getProductById(id);
    }

    @RequestMapping(value = "product", method = RequestMethod.POST)
    public Product saveProduct(@RequestBody final Product product) {
        return productService.saveProduct(product);
    }

    @RequestMapping(value = "product/delete/{id}", method = RequestMethod.GET)
    public void delete(@PathVariable final Integer id) {
        productService.deleteProduct(id);
    }
}
