package com.qbrainx;

import com.qbrainx.domain.Product;
import com.qbrainx.respository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(DataInitializer.class);

    private final ProductRepository productRepository;

    public DataInitializer(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.context.ApplicationListener#onApplicationEvent(org.
     * springframework.context.ApplicationEvent)
     */
    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        productRepository.deleteAll();
        final Product shirt = new Product();
        shirt.setId(10001);
        shirt.setDescription("Aerospike Shirt");
        shirt.setPrice(18.95);
        shirt.setImageUrl("http://hunt4freebies.com/wp-content/uploads/2014/07/Aerospike-T-shirt.png");
        shirt.setProductId("235268845711068308");
        productRepository.save(shirt);

        logger.info("Saved Shirt - id: {}", shirt.getId());

        final Product mug = new Product();
        mug.setId(10002);
        mug.setDescription("Aerospike Mug");
        mug.setPrice(4.99);

        mug.setImageUrl(
            "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR3TPM0daB-aXKfxdYqlHHgQrz67bSCPKUcpbmzPXtvo3GillfR");
        mug.setProductId("168639393495335947");
        productRepository.save(mug);

        logger.info("Saved Mug - id:" + mug.getId());
    }

}
